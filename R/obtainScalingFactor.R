#' Use mean counts and ortholog length as input for SCBN calculating scaling factor
#'
#' @param orthologTable an ortholog annotation table obtained from using obtainOrthologTable()
#' @param geneCountRef gene expression dataframe from your reference species
#' @param geneCountCompare gene expression dataframe from your compared species
#'
#' @return
#' @export
#'
#' @examples orthologTable <- obtainOrthologTable("hsapiens", "ptroglodytes")
#' scale_factor <- obtainScalingFactor(orthologTable, ref, compare)
obtainScalingFactor <- function(orthologTable, geneCountRef, geneCountCompare){

    # progress bar


    # calcuate ortholog length
    orthologTable <- orthologTable %>%
        mutate(refLength = refEnd - refStart) %>%
        mutate(compareLength = compareEnd - compareStart)

    # calcuate rowwise mean
    colnames(geneCountRef)[1] <- "refEnsemblID"
    geneCountRef <- geneCountRef %>%
        mutate(refMean = rowMeans(select(., 2:ncol(geneCountRef))))

    colnames(geneCountCompare)[1] <- "compareEnsemblID"
    geneCountCompare <- geneCountCompare %>%
        mutate(compareMean = rowMeans(select(., 2:ncol(geneCountCompare))))

    # combine dataframe
    df <- inner_join(
        orthologTable,
        geneCountRef[, c(1,ncol(geneCountRef))],
        by="refEnsemblID"
    )

    df <- inner_join(
        df,
        geneCountCompare[, c(1,ncol(geneCountCompare))],
        by="compareEnsemblID"
    )

    confidence_count <- df %>%
        filter(orthologyConfidence == 1) %>%
        nrow()

    df.scbn <- df %>% select(c(11,13,12,14))

    print(str(df))
    print(head(df.scbn)) # check

    # run scbn to obtain scaling factor
    factor <- SCBN::SCBN(orth_gene = df.scbn, hkind=1:confidence_count, a=0.05)
    scale_factor <- factor$scbn_val

    return(scale_factor)
}
