#' Title
#'
#' @param geneTable
#' @param teTable
#' @param metadata
#' @param contrast_vector
#'
#' @return
#' @export
#'
#' @examples
DEgeneTE <- function(geneTable, teTable, metadata, contrast_vector) {

    deseq2 <- function(cts, coldata) {
        if (all(rownames(coldata)==colnames(cts))) {
            dds <- DESeq2::DESeqDataSetFromMatrix(countData=cts, colData=coldata, design=~species)

            #pre-filter
            keep <- rowSums(DESeq2::counts(dds)) >= 10
            dds <- dds[keep,]

            #differentially expressed analysis
            dds <- DESeq2::DESeq(dds)
            res <- DESeq2::results(dds, contrast=contrast_vector)
            print(DESeq2::summary(res))

            #normalize
            ntd <- DESeq2::normTransform(dds)
            result_list <- list("res"=res, "ntd"=ntd)
            return(result_list)
        }
    }

    # run analysis
    gene_de <- deseq2(geneTable, metadata)
    te_de <- deseq2(teTable, metadata)

    # save files
    write.table(data.frame(gene_de$res), file="data/geneDESeq2results.csv", sep=",")
    write.table(data.frame(SummarizedExperiment::assay(gene_de$ntd)), file="data/geneDESeq2Log2.csv", sep=",")
    write.table(data.frame(te_de$res), file="data/teDESeq2results.csv", sep=",")
    write.table(data.frame(SummarizedExperiment::assay(te_de$ntd)), file="data/teDESeq2Log2.csv", sep=",")

    output <- list(
        gene_de$res,
        gene_de$ntd,
        te_de$res,
        te_de$ntd
    )
    return(output)
}









